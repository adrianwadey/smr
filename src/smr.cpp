// Simple MIDI Recorder
// Copyright (c) 2016 Adrian Wadey


// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include <locale.h>

#include "smr.h"
#include "forms/libsmrmainform.h"
#include "i18n.h"

wxIMPLEMENT_APP(Smr);

bool Smr::OnInit()
{
    setlocale(LC_ALL, "");
    bindtextdomain(PACKAGE, LOCALEDIR);
    textdomain(PACKAGE);
    printf(i18n("hello world!\n"));
    printf(i18n("hello POT Test!\n"));
    FrameSMR *frame = new FrameSMR( NULL );
    frame->Show( true );
    return true;
}

