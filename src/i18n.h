
#ifndef __i18n_h__
#define __i18n_h__
#include "../config.h"
#include "gettext.h"
#define i18n(text) gettext(text)
#endif //__i18n_h__
