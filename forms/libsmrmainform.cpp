///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "libsmrmainform.h"

///////////////////////////////////////////////////////////////////////////

FrameSMR::FrameSMR( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	m_statusBarSMR = this->CreateStatusBar( 1, wxST_SIZEGRIP, wxID_ANY );
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );
	
	m_buttonSave = new wxButton( this, wxID_ANY, wxT("S&ave"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1->Add( m_buttonSave, 0, wxALL, 5 );
	
	m_textFileName = new wxTextCtrl( this, wxID_ANY, wxT("MIDIFile.MID"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1->Add( m_textFileName, 0, wxALL, 5 );
	
	m_buttonPlay = new wxButton( this, wxID_ANY, wxT("&Play"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1->Add( m_buttonPlay, 0, wxALL, 5 );
	
	m_buttonRecord = new wxButton( this, wxID_ANY, wxT("&Record"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1->Add( m_buttonRecord, 0, wxALL, 5 );
	
	m_buttonStop = new wxButton( this, wxID_ANY, wxT("&Stop"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1->Add( m_buttonStop, 0, wxALL, 5 );
	
	m_button5 = new wxButton( this, wxID_ANY, wxT("MyButton"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer1->Add( m_button5, 0, wxALL, 5 );
	
	
	this->SetSizer( bSizer1 );
	this->Layout();
	
	this->Centre( wxBOTH );
}

FrameSMR::~FrameSMR()
{
}
