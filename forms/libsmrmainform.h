///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 17 2015)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __LIBSMRMAINFORM_H__
#define __LIBSMRMAINFORM_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/statusbr.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/string.h>
#include <wx/button.h>
#include <wx/textctrl.h>
#include <wx/sizer.h>
#include <wx/frame.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class FrameSMR
///////////////////////////////////////////////////////////////////////////////
class FrameSMR : public wxFrame 
{
	private:
	
	protected:
		wxStatusBar* m_statusBarSMR;
		wxButton* m_buttonSave;
		wxTextCtrl* m_textFileName;
		wxButton* m_buttonPlay;
		wxButton* m_buttonRecord;
		wxButton* m_buttonStop;
		wxButton* m_button5;
	
	public:
		
		FrameSMR( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Simple MIDI Recorder"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		
		~FrameSMR();
	
};

#endif //__LIBSMRMAINFORM_H__
