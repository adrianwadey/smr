# English translations for smr package
# English (United Kingdom) translations for smr package.
# Copyright (C) 2016 Adrian Wadey
# This file is distributed under the same license as the smr package.
# adrianw <adrianwadey@yahoo.com>, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: smr 0.1.0\n"
"Report-Msgid-Bugs-To: adrianwadey@yahoo.com\n"
"POT-Creation-Date: 2016-02-12 00:30+0000\n"
"PO-Revision-Date: 2016-02-12 00:34+0000\n"
"Last-Translator: adrianw <adrianwadey@yahoo.com>\n"
"Language-Team: English (British) <en_gb@li.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: src/smr.cpp:24
msgid "hello world!\n"
msgstr "hello world!\n"
